var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var path = require('path');

var env = process.env.WEBPACK_ENV || 'dev';

var config = {
  entry: {
    main: ['whatwg-fetch', './src/main-entry.jsx']
  },

  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      }
    ]
  },

  resolve: {
    cache: false, // set to true when in test mode
    root: root(),
    extensions: ['', '.jsx', '.js', '.json', '.css', '.scss', '.html'],
    alias: {
      'app': 'src/app',
      'common': 'src/common'
    }
  },

  plugins: [
    new HtmlWebpackPlugin({
      inject: false,
      filename: 'index.html',
      template: 'static/index_template.html'
    })
  ],
  output: {
    path: path.join(__dirname, 'dist')
  }
};

if (env === 'dev') {
  config.plugins.push(new webpack.HotModuleReplacementPlugin());

  config.output.filename = '[name].js';
  config.output.chunkFilename = '[id].chunk.js';
  config.output.publicPath = '/static/';

  config.devtool = 'eval-source-map'; // or 'source-map' if you want slightly better debugging functionality in chrome
}

if (env === 'build') {
  config.plugins.push(new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production')
    }
  }));
  config.plugins.push(new webpack.optimize.OccurenceOrderPlugin());
  config.plugins.push(new webpack.optimize.DedupePlugin());
  config.plugins.push(new webpack.optimize.UglifyJsPlugin({
    minimize: true,
    compress: {
      warnings: false
    },
    sourceMaps: false
  }));

  config.output.filename = '[name].[hash].js';
  config.output.chunkFilename = '[id].[hash].chunk.js';
  config.output.publicPath = '/';

  //config.devtool = 'source-map';
}

module.exports = config;

// Helper functions
function root(args) {
  args = Array.prototype.slice.call(arguments, 0);
  return path.join.apply(path, [__dirname].concat(args));
}
