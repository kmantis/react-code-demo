export function getAssignments() {
  return fetch('http://localhost:8080/static/assignments.json')
    .then((response) => {
      if (!response) {
        return Promise.reject("Kon geen verbinding krijgen");
      }
      return response.json();
    });
}
