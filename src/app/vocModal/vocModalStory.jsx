import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import VocModal from "./vocModal.jsx"

storiesOf('VocModal', module)
  .add('Displaying', () => (
    <div>
      <VocModal show={true} onRequestClose={action('requestClose')}>
        Hello there gentlemen!<br/>
      </VocModal>
    </div>
  ))
  .add('Not displaying', () => (
    <div>
      <VocModal show={false} onRequestClose={action('requestClose')}>
        Hello there gentlemen!<br/>
      </VocModal>
    </div>
  ));
