import React, {PropTypes} from 'react';
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

const VocModal = React.createClass({
  propTypes: {
    show: PropTypes.bool,
    onRequestClose: PropTypes.func.isRequired
  },

  onClose(e) {
    e.preventDefault();
    e.stopPropagation();
    this.props.onRequestClose();
  },

  renderBackdrop () {
    if (this.props.show) {
      return (<div key="backdrop-show"
                   className="vocModalBg"
                   onClick={this.onClose}/>);
    } else {
      return (<div key="backdrop-hide"/>);
    }
  },

  renderModalWindow () {
    if (this.props.show) {
      return (
        <div key="vocModal-show"
             className="fixed-fullscreen-container"
        style={{zIndex: '100'}}>
          <div
            className="vocModal"
            onClick={this.onClose}>
            {this.props.children}
          </div>
        </div>
      );
    } else return (<div key="vocModal-hide"/>);
  },

  render () {
    return (
      <div onClick={this.onClose}>
        <ReactCSSTransitionGroup component="div"
                                 transitionName="vocModalBg" transitionAppear={true}
                                 transitionAppearTimeout={1000}
                                 transitionEnterTimeout={1000}
                                 transitionLeaveTimeout={700}>
          {this.renderBackdrop()}
        </ReactCSSTransitionGroup>
        <div style={{
          position: 'relative',
          height: '100%', width: '100%'
        }}>
          <ReactCSSTransitionGroup component="div"
                                   transitionName="vocModal" transitionAppear={true}
                                   transitionAppearTimeout={500}
                                   transitionEnterTimeout={500}
                                   transitionLeaveTimeout={500}>
            {this.renderModalWindow()}
          </ReactCSSTransitionGroup>
        </div>

      </div>
    )
  }
});

export default VocModal;
