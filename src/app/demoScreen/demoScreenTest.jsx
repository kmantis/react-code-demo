import React from 'react';
import {mount} from 'enzyme';
import DemoScreen from './demoScreen'

const assignmentMockData = [ {id: 1, name: 'assignment 1', isFinished: true}, {id: 2, name: 'assignment 2', isFinished: false} ];

const getAssignmentsMock = (callback, shouldResolve, data) => {
  setTimeout(callback, 1);
  return shouldResolve ? Promise.resolve(data) : Promise.reject('an error occured');
};

describe('DemoScreen', () => {
  let screen;

  describe('When assignments are returned by the given service', () => {

    beforeEach((done) => {
      screen = mount(<DemoScreen getAssignments={() => { return getAssignmentsMock(done, true, assignmentMockData); } }/>);
    });

    it('Renders a list of assignments', () => {
      const assignments = screen.find('Assignment');
      expect(assignments.length).toBe(2);

      const firstAssignmentProps = assignments.get(0).props,
        secondAssignmentProps = assignments.get(1).props;

      expect(firstAssignmentProps.id).toBe(1);
      expect(firstAssignmentProps.name).toBe('assignment 1');
      expect(firstAssignmentProps.isFinished).toBe(true);

      expect(secondAssignmentProps.id).toBe(2);
      expect(secondAssignmentProps.name).toBe('assignment 2');
      expect(secondAssignmentProps.isFinished).toBe(false);
    });

  });

  describe('When an empty list is returned by the given service', () => {
    beforeEach((done) => {
      screen = mount(<DemoScreen getAssignments={() => { return getAssignmentsMock(done, true, []); } }/>);
    });

    it('Renders no assignments', () => {
      const assignments = screen.find('Assignment');
      expect(assignments.length).toBe(0);
    });
  });

  describe('When the getAssignments function returns null', () => {
    beforeEach((done) => {
      screen = mount(<DemoScreen getAssignments={() => { return getAssignmentsMock(done, true, null); } }/>);
    });

    it('Renders no assignments, without throwing errors', () => {
      const assignments = screen.find('Assignment');
      expect(assignments.length).toBe(0);
    });
  });

  describe('When an error occurs when retrieving assignments', () => {
    beforeEach((done) => {
      screen = mount(<DemoScreen getAssignments={() => { return getAssignmentsMock(done, false, []); } }/>);
    });

    it('Renders no assignments', () => {
      const assignments = screen.find('Assignment');
      expect(assignments.length).toBe(0);
    });
  });

});
