import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import DemoScreen from "./demoScreen.jsx"

const assignments = [
  { "id": 10, "name": "My first assignment", "isFinished": true },
  { "id": 20, "name": "My second assignment", "isFinished": false },
  { "id": 30, "name": "My third assignment", "isFinished": true },
  { "id": 40, "name": "My fourth assignment", "isFinished": false }
];

const generateGetAssignmentsFn = (shouldResolve, delay) => {
  return () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        shouldResolve ? resolve(assignments) : reject("some error");
      }, delay);
    });
  }
};

storiesOf('DemoScreen', module)
  .add('With data', () => (
    <div>
      <DemoScreen getAssignments={generateGetAssignmentsFn(true)}/>
    </div>
  ))
  .add('With error', () => (
    <div>
      <DemoScreen getAssignments={generateGetAssignmentsFn(false)}/>
    </div>
  ))
  .add('With delay', () => (
    <div>
      <DemoScreen getAssignments={generateGetAssignmentsFn(true, 3000)}/>
    </div>
  ));
