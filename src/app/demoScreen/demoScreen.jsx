import React, {PropTypes} from 'react'
import PreloadingIndicator from '../loadingIndicator/preloadingIndicator'
import Assignment from '../assignment/assignment'

const DemoScreen = React.createClass({
  propTypes: {
    getAssignments: PropTypes.func.isRequired
  },

  getInitialState () {
    return {
      assignments: []
    }
  },

  loadData() {
    return this.props.getAssignments()
      .then(results => {
        this.setState( {assignments: results} );
      });
  },

  renderAssignments() {
    return (this.state.assignments || []).map(assignment => {
      return (
        <Assignment key={'assignment-' + assignment.id} id={assignment.id} name={assignment.name} isFinished={assignment.isFinished} />
      );
    });
  },

  render () {
    return (
      <PreloadingIndicator loadFunc={this.loadData}>
        {this.renderAssignments()}
      </PreloadingIndicator>
    )
  }
});

export default DemoScreen;
