import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import Assignment from "./assignment.jsx"

storiesOf('Assignment', module)
  .add('Finished', () => (
    <div>
      <Assignment id={123} name="My assignment name" isFinished={true}/>
    </div>
  ))
  .add('Not finished', () => (
    <div>
      <Assignment id={456} name="My other assignment name" isFinished={false}/>
    </div>
  ));
