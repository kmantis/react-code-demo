import React from 'react';
import {shallow} from 'enzyme';
import Assignment from './assignment'

describe('Assignment', () => {

  it('renders an unfinished assignment', () => {
    const component = shallow(<Assignment id={1} name="my name" isFinished={false}/>);

    expect(component.text()).toContain('my name');
    expect(component.find('img').props().src).toBe('css/img/treasure-map.svg');
  });

  it('renders a finished assignment', () => {
    const component = shallow(<Assignment id={1} name="my name" isFinished={true}/>);

    expect(component.text()).toContain('my name');
    expect(component.find('img').props().src).toBe('css/img/arrow-checkmark.svg');
  });
});
