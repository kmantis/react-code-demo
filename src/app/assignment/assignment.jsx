import React, {PropTypes} from 'react'

const Assignment = React.createClass({
  propTypes: {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    isFinished: PropTypes.bool.isRequired
  },

  render () {
    return (
      <div style={{padding: '10px'}}>
        <a href='#'>
          <img height="70" style={{verticalAlign: 'middle', paddingRight: '10px'}}
               src={this.props.isFinished ? "css/img/arrow-checkmark.svg" : "css/img/treasure-map.svg"}/>
          <div className="brown-bordered">
            {this.props.name}
          </div>
        </a>
      </div>
    )
  }
});

export default Assignment;
