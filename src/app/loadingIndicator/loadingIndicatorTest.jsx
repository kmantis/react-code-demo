import React from 'react';
import {shallow} from 'enzyme';
import LoadingIndicator from './loadingIndicator'

describe('LoadingIndicator', () => {

  it('renders an indicator', () => {
    const component = shallow(<LoadingIndicator display={true}/>);
    expect(component.find('.bullet').length).toBe(4);
    expect(component.props().style.display).toBe('block');
  });

  it('hides an indicator when display is false', () => {
    const component = shallow(<LoadingIndicator display={false}/>);
    expect(component.find('.bullet').length).toBe(4);
    expect(component.props().style.display).toBe('none');
  });
});
