import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import PreloadingIndicator from "./preloadingIndicator.jsx"

const generatePromise = (delay, shouldResolve, rejectMessage) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => { shouldResolve ? resolve() : reject(rejectMessage) }, delay);
  });
};

storiesOf('PreloadingIndicator', module)
  .add('With 5 sec delay', () => (
    <div>
      <PreloadingIndicator
        loadFunc={() => {return generatePromise(5000, true)}}
      >
        Load succesful!
      </PreloadingIndicator>
    </div>
  ))
  .add('Without delay', () => (
    <div>
      <PreloadingIndicator
        loadFunc={() => {return generatePromise(0, true)}}
      >
        Load succesful!
      </PreloadingIndicator>
    </div>
  ))
  .add('With rejected promise after 3 seconds delay', () => (
    <div>
      <PreloadingIndicator
        loadFunc={() => {return generatePromise(3000, false, 'Could not load things')}}
      >
        Load succesful, but should'nt be :(
      </PreloadingIndicator>
    </div>
  ))
  .add('With rejected promise after no delay', () => (
    <div>
      <PreloadingIndicator
        loadFunc={() => {return generatePromise(1, false, 'Could not load things')}}
      >
        Load succesful, but should'nt be :(
      </PreloadingIndicator>
    </div>
  ));
