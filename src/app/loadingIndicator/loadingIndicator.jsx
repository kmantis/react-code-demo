import React, { PropTypes } from 'react'

const LoadingIndicator = React.createClass({
  propTypes: {
    display: PropTypes.bool.isRequired
  },

  render () {
    return (
      <div className='loadingIndicator' style={{ display: this.props.display ? 'block': 'none'}}>
        <div className='bullet'></div>
        <div className='bullet'></div>
        <div className='bullet'></div>
        <div className='bullet'></div>
      </div>
    )
  }
});

export default LoadingIndicator;
