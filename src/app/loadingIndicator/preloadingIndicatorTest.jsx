import React from 'react';
import {mount} from 'enzyme';
import PreloadingIndicator from './preloadingIndicator'

const generateLoadFunc = (callback, shouldResolve) => {

  return () => {
    setTimeout(callback, 2);
    return shouldResolve ? Promise.resolve({something: true}) : Promise.reject('an error occured');
  }
};

describe('PreloadingIndicator', () => {
  describe('when the loadFunc\'s promise is resolved', () => {
    let component;

    beforeEach((done) => {
      component = mount(
        <PreloadingIndicator loadFunc={generateLoadFunc(done, true)}>
          <div id="something">yes</div>
        </PreloadingIndicator>);
    });

    it('renders the children', () => {
      const children = component.find('#something');
      expect(children.length).toBe(1);
    });
  });


  describe('while the loadFunc\'s promise is not yet resolved', () => {

    afterEach(() => {
      jest.useRealTimers();
    });

    it('renders a loading indicator after a few seconds', () => {
      jest.useFakeTimers();
      const component = mount(
        <PreloadingIndicator
          loadFunc={() => {
            return new Promise(() => {
            });
          }}>
          <div id="something">yes</div>
        </PreloadingIndicator>),
        children = component.find('#something'),
        loadingIndicator = component.find('LoadingIndicator');

      expect(children.length).toBe(0);
      expect(loadingIndicator.length).toBe(1);
      expect(loadingIndicator.props().display).toBe(false);

      jest.runAllTimers();
      expect(loadingIndicator.props().display).toBe(true);
    });
  });

  describe('when the loadFunc\'s promise is rejected', () => {
    let component;
    beforeEach((done) => {
      component = mount(
        <PreloadingIndicator loadFunc={generateLoadFunc(done, false)}>
          <div id="something">yes</div>
        </PreloadingIndicator>);
    });

    it('renders an error message', () => {
        const children = component.find('#something'),
        loadingIndicator = component.find('LoadingIndicator'),
        modal = component.find('VocModal');

      expect(children.length).toBe(0);
      expect(loadingIndicator.props().display).toBe(false);
      expect(modal.props().show).toBe(true);
    });
  });

});
