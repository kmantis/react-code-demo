import React, {PropTypes} from 'react';
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import LoadingIndicator from './loadingIndicator'
import VocModal from '../vocModal/vocModal'

const PreloadingIndicator = React.createClass({
  propTypes: {
    loadFunc: PropTypes.func.isRequired
  },

  getInitialState () {
    return {
      displayIndicator: false,
      displayChildren: false,
      errorMessage: null
    }
  },

  componentDidMount () {
    this.isMounted = true;
    this.initiateLoading();
  },

  componentWillUnmount () {
    this.isMounted = false;
  },

  initiateLoading () {
    this.setState({displayChildren: false, errorMessage: null, displayIndicator: false});

    const
      timeoutId = setTimeout(() => {
        if (this.isMounted) { this.setState({displayIndicator: true}); }
      }, 1500),
      promise = this.props.loadFunc();

    if (!promise.then) {
      console.warn('PreloadingIndicator\'s `promise` function did not return a thennable promise');
      this.handleLoadResult(0, true);
    }

    promise.then(() => {
      this.handleLoadResult(timeoutId, true);
    }).catch(message => {
      this.handleLoadResult(timeoutId, false, message);
    });
  },

  handleLoadResult(timeoutId, wasSuccessfull, errorMessage) {
    if (!this.isMounted) { return; }

    clearTimeout(timeoutId);

    if (wasSuccessfull) {
      this.setState({displayChildren: true, errorMessage: null, displayIndicator: false});
    } else {
      this.setState({
        displayChildren: false,
        displayIndicator: false,
        errorMessage: 'Er is helaas iets fout gegaan.'
      });
    }
  },

  render () {
    const elements = this.state.displayChildren ?
      (<div key="children">{this.props.children}</div>)
      :
      (<LoadingIndicator key="loading-indicator" display={this.state.displayIndicator}/>);

    return (
      <ReactCSSTransitionGroup transitionName="opacity"
                               transitionAppear={true}
                               transitionAppearTimeout={500} transitionEnterTimeout={500}
                               transitionLeaveTimeout={500}>
        {elements}
        <VocModal show={!!this.state.errorMessage} onRequestClose={() => {}}>
          <div className="preloadingIndicator-error">
            {this.state.errorMessage}<br/><br/>
            <span className="preloadingIndicator-error-options">
              <a onClick={() => { history.back(); }}>ga naar vorige pagina</a> of <a onClick={() => { this.initiateLoading(); }}>probeer opnieuw</a>
            </span>
          </div>
        </VocModal>
      </ReactCSSTransitionGroup>
    )
  }
});

export default PreloadingIndicator;
