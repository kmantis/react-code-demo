import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import LoadingIndicator from "./loadingIndicator.jsx"

storiesOf('LoadingIndicator', module)
  .add('Displaying', () => (
    <div>
      <LoadingIndicator display={true}/>
    </div>
  ))
  .add('Not displaying', () => (
    <div>
      <LoadingIndicator display={false}/>
    </div>
  ));;
