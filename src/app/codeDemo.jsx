import React from "react";
import DemoScreen from './demoScreen/demoScreen'
import {getAssignments} from "./service/assignmentService";

const CodeDemo = React.createClass({
  render () {
    return (
      <div>
        <h1>'Op jacht naar de Woordenschat! code structure demo</h1>
        <DemoScreen getAssignments={getAssignments}/>
      </div>
    )
  }
});

export default CodeDemo;
