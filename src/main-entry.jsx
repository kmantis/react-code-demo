import React from "react";
import ReactDOM from "react-dom";
import CodeDemo from 'app/codeDemo'

ReactDOM.render(
  <CodeDemo/>, document.getElementById('main')
);
