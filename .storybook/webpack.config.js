const path = require('path');

module.exports = (storybookBaseConfig, configType) => {
  // configType has a value of 'DEVELOPMENT' or 'PRODUCTION'
  // You can change the configuration based on that.
  // 'PRODUCTION' is used when building the static version of storybook.

  // Make whatever fine-grained changes you need
  storybookBaseConfig.resolve = {
    extensions: ['', '.jsx', '.js', '.json', '.css', '.scss', '.html'],
    alias: {
      'app': 'src/app',
      'common': 'src/common'
    }
  };

  storybookBaseConfig.module.loaders.push( {
    test: /\.css?$/,
    loaders: [ 'style', 'raw' ],
    include: path.resolve(__dirname, '../static')
  });

  // Return the altered config
  return storybookBaseConfig;
};
