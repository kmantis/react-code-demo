import { configure } from '@kadira/storybook';
import '../static/css/codeDemo.css'

const req = require.context('../src', true, /Story\.jsx$/);

function loadStories() {
  req.keys().forEach(req)
}

configure(loadStories, module);
