# Code structure demo

## Running locally

* Start local server: `npm start`
* [http://localhost:8080/static/index.html](http://localhost:8080/static/index.html)

## Component storybook

* Run `npm run storybook`
* [http://localhost:8081](http://localhost:8081)
